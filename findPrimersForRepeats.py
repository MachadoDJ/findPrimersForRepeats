#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# findPrimersForRepeats.py

# Selected satellites: Chap4sat_Xt, MSAT2_XT, MSAT4_XT, REM2b_Xt, Sat1_Xt, Tc1Sat1_Xt, and
# URR1a_Xt.
# Selected retroposons: SINE, MIR, L1, CIN4, L2, CR1, REX, and Penelope.

# This program requires a working installation of Primer3 (tested with libprimer3 release
# 2.3.7)

# Author: Denis Jacob Machado
# Email: machadodj<at>usp.br

#########################
### LIBRARIES/ MODULES ##
#########################

# Set arguments
import argparse,re,sys,subprocess
parser=argparse.ArgumentParser()
# Main arguments
parser.add_argument("-f","--fasta",help="Masked sequences (MULTIFASTA)",type=str,required=True)
parser.add_argument("-g","--gff",help="Annotations (GFF)",type=str,required=True)
# Optional arguments
parser.add_argument("-c","--config",help="Path to directory with primer thermodynamic parameters",type=str,required=True)
parser.add_argument("-o","--output",help="Output prefix",type=str,default="output",required=False)
parser.add_argument("-p","--primer3",help="Path to primer3_core executable",type=str,required=True)
# Modes
parser.add_argument("-r","--retroposons",help="Find primers for TEs (retroposons)",action="store_true",default=False,required=False)
parser.add_argument("-s","--satellites",help="Find primers for satellites",action="store_true",default=True,required=False) # Default
args=parser.parse_args()

######################################
### SPECIFIC FUNCTIONS - SATELLITES ##
######################################

def readGff_S(file):
	sats={}
	types=[]
	handle=open(file,"rU")
	for line in handle.readlines():
		line=line.strip()
		if("Chap4sat_Xt" in line)or("MSAT2_XT" in line)or("MSAT4_XT" in line)or("REM2b_Xt" in line)or("Sat1_Xt" in line)or("Tc1Sat1_Xt" in line)or("URR1a_Xt" in line)or(re.search("Motif:\([A-Z][A-Z][A-Z][A-Z].*\)n",line)): # Search keywords that identify Satellites (based on all satellite types in RepeatMasker .out file).
			line=line.split()
			seqname=line[0] # Get sequence name (repeat id)
			start=int(line[3]) # Start position (start >= 1)
			end=int(line[4]) # End position
			score=float(line[5]) # Score (not always reliable)
			strand=line[6] # Strand (+ or -)
			motif=re.sub("Motif:","",line[9]) # Clean up a little
			motif=re.sub("[\'\"\s]","",motif)
			motif=re.sub("\(","",motif)
			motif=re.sub("\)n","",motif)
			if(seqname in sats): # Let's skip any sequence repeat with more than 2 SINEs - they might be complicate to analyse and require manual curation,
				sys.stderr.write("! WARN: igoring sequence with multiple satellites ({}).\n".format(seqname))
				del sats[seqname]
			elif((end-start+1)<=150): # Only short satellites up to 150  are accepted
				sats[seqname]=[start,end,score,strand,motif] # Add satellite to dictionary
			types.append(motif) # Store general categories of satellites
	handle.close()
	sys.stdout.write("Pre-selected {} satellites of {} types:\n".format(len(sats),len(set(types))))
	for i in sorted(list(set(types))):
		sys.stdout.write("{}, n={}\n".format(i,types.count(i)))
	sys.stdout.write("//\n")
	return sats

#————————————————————————————————————————————————————————————————————————————————————————#

def runPrimer3_S(selected,primer3,config):
	sys.stdout.write("Name\tType of TE\tQuery sequence\n")
	for seqname in sorted(selected):
		seq  =selected[seqname][0]
		start=selected[seqname][1]
		end  =selected[seqname][2]
		motif=selected[seqname][3]
		length=end-start+1
# 		min=length+36 # Minimum product size = target size plus two times min. primer size
# 		max=length+80 #  Maximum product size = target size five times max. primer size
		prefix="{}_{}".format(seqname,motif)
		input="""SEQUENCE_ID={}
SEQUENCE_TEMPLATE={}
SEQUENCE_TARGET={},{}
PRIMER_TASK=generic
PRIMER_PICK_LEFT_PRIMER=1
PRIMER_PICK_INTERNAL_OLIGO=0
PRIMER_PICK_RIGHT_PRIMER=1
PRIMER_NUM_RETURN=1
PRIMER_MIN_TM=58
PRIMER_OPT_TM=60
PRIMER_MAX_TM=62
PRIMER_PAIR_MAX_DIFF_TM=2
PRIMER_MIN_GC=40
PRIMER_OPT_GC=50
PRIMER_MAX_GC=60
PRIMER_GC_CLAMP=0
PRIMER_MAX_POLY_X=4
PRIMER_MIN_SIZE=17
PRIMER_OPT_SIZE=20
PRIMER_MAX_SIZE=24
PRIMER_MAX_NS_ACCEPTED=1
PRIMER_PRODUCT_SIZE_RANGE={}-{}
P3_FILE_FLAG=0
PRIMER_EXPLAIN_FLAG=1
PRIMER_LOWERCASE_MASKING=1
PRIMER_THERMODYNAMIC_PARAMETERS_PATH={}
=
""".format(prefix,seq,start,(end-start)+1,80,150,config)
		handle=open("configuration.temp","w")
		handle.write(input)
		handle.close()
		result=subprocess.run("{} < configuration.temp".format(primer3),shell=True,stdout=subprocess.PIPE).stdout.decode('utf-8')
		subprocess.run("rm configuration.temp",shell=True)
		processResult(result)
	sys.stdout.write("//\n")
	return

#######################################
### SPECIFIC FUNCTIONS - RETROPOSONS ##
#######################################

def readGff_R(file):
	tes={}
	types=[]
	handle=open(file,"rU")
	for line in handle.readlines():
		line=line.strip()
		if("SINE" in line)or("Motif:MIR" in line)or("Motif:L1" in line)or("CIN4" in line)or("Motif:L2" in line)or("CR1" in line)or("REX" in line)or("Penelope" in line): # Search keywords that identify SINEs, MIRs, Penelopes, or some LINEs
			line=line.split()
			seqname=line[0] # Get sequence name (repeat id)
			start=int(line[3]) # Start position (start >= 1)
			end=int(line[4]) # End position
			score=float(line[5]) # Score (not always reliable)
			strand=line[6] # Strand (+ or -)
			motif=re.sub("Motif:","",line[9]) # Clean up a little
			motif=re.sub("[\'\"\s]","",motif)
			if(seqname in tes): # Let's skip any sequence repeat with more than 2 SINEs - they might be complicate to analyse and require manual curation,
				sys.stderr.write("! WARN: igoring sequence with multiple SINEs ({}).\n".format(seqname))
				del tes[seqname]
			elif((end-start+1)<=300): # Only short TEs up to 300bp are accepted
				tes[seqname]=[start,end,score,strand,motif] # Add SINE/ MIR to dictionary
			types.append(motif) # Store general categories of SINEs/ MIRs
	handle.close()
	sys.stdout.write("//\nPre-selected {} TEs of {} types:\n".format(len(tes),len(set(types))))
	for i in sorted(list(set(types))):
		sys.stdout.write("{}, n={}\n".format(i,types.count(i)))
	sys.stdout.write("//\n")
	return tes

#————————————————————————————————————————————————————————————————————————————————————————#

def runPrimer3_R(selected,primer3,config):
	sys.stdout.write("Name\tType of TE\tQuery sequence\n")
	for seqname in sorted(selected):
		seq  =selected[seqname][0]
		start=selected[seqname][1]
		end  =selected[seqname][2]
		motif=selected[seqname][3]
		length=end-start+1
		min=length+36 # Minimum product size = target size plus two times min. primer size
		max=length+80 #  Maximum product size = target size five times max. primer size
		prefix="{}_{}".format(seqname,motif)
		input="""SEQUENCE_ID={}
SEQUENCE_TEMPLATE={}
SEQUENCE_TARGET={},{}
PRIMER_TASK=generic
PRIMER_PICK_LEFT_PRIMER=1
PRIMER_PICK_INTERNAL_OLIGO=0
PRIMER_PICK_RIGHT_PRIMER=1
PRIMER_NUM_RETURN=1
PRIMER_MIN_TM=57
PRIMER_OPT_TM=60
PRIMER_MAX_TM=63
PRIMER_PAIR_MAX_DIFF_TM=2
PRIMER_MIN_GC=20
PRIMER_OPT_GC=50
PRIMER_MAX_GC=80
PRIMER_GC_CLAMP=0
PRIMER_MAX_POLY_X=5
PRIMER_MIN_SIZE=18
PRIMER_OPT_SIZE=20
PRIMER_MAX_SIZE=27
PRIMER_MAX_NS_ACCEPTED=1
PRIMER_PRODUCT_SIZE_RANGE={}-{}
P3_FILE_FLAG=0
PRIMER_EXPLAIN_FLAG=1
PRIMER_LOWERCASE_MASKING=1
PRIMER_THERMODYNAMIC_PARAMETERS_PATH={}
=
""".format(prefix,seq,start,(end-start)+1,min,max,config)
		handle=open("configuration.temp","w")
		handle.write(input)
		handle.close()
		result=subprocess.run("{} < configuration.temp".format(primer3),shell=True,stdout=subprocess.PIPE).stdout.decode('utf-8')
		subprocess.run("rm configuration.temp",shell=True)
		processResult(result)
	sys.stdout.write("//\n")
	return

#######################
## GENERAL FUNCTIONS ##
#######################

def readMasked(file,sats):
	selected={}
	handle=open(file,"rU")
	length=[]
	for entry in re.compile(">([^\n]+?)\n([^>]+)",re.M|re.S).findall(handle.read()): # Read one fasta entry at a time in the multifasta file
		seqname=entry[0].strip()
		seq=re.sub("[\s\n]","",entry[1].strip())
		if(seqname in sats):
			start=sats[seqname][0]
			end=sats[seqname][1]
			strand=sats[seqname][3]
			motif=sats[seqname][4]
			before=seq[start-1-24:start]
			after=seq[end:end+24]
			if(sum(1 for c in before if c.isupper())<24 or sum(1 for c in after if c.isupper())<24): # Accept repeats if there is at lest 24 non-masked nucleotides on each end
				pass
			else:
				length+=[end-start+1]
				if(strand=="+"): # If positive strand, do nothing
					selected[seqname]=[seq,start,end,motif]
				else: # Else, reverse compliment and recalculate start and end positions
					seq=reverse_complement(seq)
					size=len(seq)
					x=(size-end)+1
					y=(size-end)+(end-start)+1
					start=x
					end=y
					selected[seqname]=[seq,start,end,motif]
	handle.close()
	sys.stdout.write("Selected {} different satellites\n".format(len(selected)))
	sys.stdout.write("Size: min={}, max={}, avg.={}\n//\n".format(min(length),max(length),float(sum(length))/float(len(length))))
	return selected

#————————————————————————————————————————————————————————————————————————————————————————#

def reverse_complement(seq):
	complement = {"A":"T","T":"A","U":"A","G":"C","C":"G","Y":"R","R":"Y","S":"S","W":"W","K":"M","M":"K","B":"V","D":"H","H":"D","V":"B","N":"N","a":"t","t":"a","u":"a","g":"c","c":"g","y":"r","r":"y","s":"s","w":"w","k":"m","m":"k","b":"v","d":"h","h":"d","v":"b","n":"n"}
	bases=list(seq)
	bases=reversed([complement.get(base,base) for base in bases])
	bases=''.join(bases)
	return bases

#————————————————————————————————————————————————————————————————————————————————————————#

def processResult(result):
	id=re.compile("SEQUENCE_ID=([^\s\n]+)").findall(result)[0]
	try:
		dna=re.compile("SEQUENCE_TEMPLATE=([^\s\n]+)").findall(result)[0]
		p1=re.compile("PRIMER_LEFT_0_SEQUENCE=([^\s\n]+)").findall(result)[0]
		p2=re.compile("PRIMER_RIGHT_0_SEQUENCE=([^\s\n]+)").findall(result)[0]
		t1=float(re.compile("PRIMER_LEFT_0_TM=([^\s\n]+)").findall(result)[0])
		t2=float(re.compile("PRIMER_RIGHT_0_TM=([^\s\n]+)").findall(result)[0])
		s1,l1=[int(x) for x in re.compile("PRIMER_LEFT_0=(\d+),(\d+)").findall(result)[0]]
		s2,l2=[int(x) for x in re.compile("PRIMER_RIGHT_0=(\d+),(\d+)").findall(result)[0]]
		output=""">{0} {1}:{2}
{3}
>{0}_L {1}:{4} TM={8}
{5}
>{0}_R {2}:{6} TM={9}
{7}
""".format(id,s1,s2,dna[s1:s2+1],s1+l1,p1,s2-l2,p2,t1,t2)
		seqname,te=re.compile("(.+?)_(.+)").findall(id)[0]
		handle=open("{}.{}.{}.fasta".format(args.output,te,seqname),"w")
		handle.write(output)
		handle.close()
		sys.stdout.write("{}\t{}\t{}\n".format(args.output,te,seqname))
	except:
		sys.stderr.write("! WARN: {} failed.\n".format(id))
		pass
	return

#######################
## EXECUTE FUNCTIONS ##
#######################

if(args.satellites):
	sats=readGff_S(args.gff)
	selected=readMasked(args.fasta,sats)
	runPrimer3_S(selected,args.primer3,args.config)

#————————————————————————————————————————————————————————————————————————————————————————#

if(args.retroposons):
	tes=readGff_R(args.gff)
	selected=readMasked(args.fasta,tes)
	runPrimer3_R(selected,args.primer3,args.config)

##########
## QUIT ##
##########

exit()
