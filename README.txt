findPrimersForRepeats.py - Copyright (C) 2017 - Denis Jacob Machado

version 20170924

——————————————————————————————————————————————————————————————————————————————————————————

i. AVAILABILITY

findPrimersForRepeats.py comes with ABSOLUTELY NO WARRANTY! findPrimersForRepeats.py is
available at www.ib.usp.br/grant/anfibios/researchSoftware.html.

The programs may be freely used, modified, and shared under the GNU General Public License
version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).

See LICENSE.txt for complete information about terms and conditions.

findPrimersForRepeats.py is also available at GitLab: https://gitlab.com/MachadoDJ/

Also, check the wiki page that are available at GitLab.

——————————————————————————————————————————————————————————————————————————————————————————

ii. CONTACT

Author: Denis Jacob Machado
Homepage: http://about.me/machadodj
Email: machadodj<at>usp.br
Our lab's website: http://www.ib.usp.br/grant/anfibios

——————————————————————————————————————————————————————————————————————————————————————————

iii. SUMMARY

This program takes a MULTIFASTA file with repetitive DNA masked (lowercase) with
RepeatMasked. It also takes an annotation file in GFF format from RepeatMasker.

This program finds primers for repetitive DNA sequences according to the following
criteria:

Retroposons:
• Accepted types: SINE, MIR, L1, CIN4, L2, CR1, REX, and Penelope.
• Flanking unmasked DNA: >=24bp each end.
• Repeat size: <=300bp.
• TM: 57—63ºC, optimal of 60ºC.
• Maximum difference in TM = 2ºC.
• GC content: 20—80%, optimal of 50%.
• Primer size: 18—27bp, optimal of 20bp.

Satellites:
• Accepted types: SINE, MIR, L1, CIN4, L2, CR1, REX, and Penelope.
• Flanking unmasked DNA: >=24bp each end.
• Repeat size: <=150bp.
• TM: 58—62ºC, optimal of 60ºC.
• Maximum difference in TM = 2ºC.
• GC content: 40—60%, optimal of 50%.
• Primer size: 17—24bp, optimal of 20bp.

——————————————————————————————————————————————————————————————————————————————————————————

1. DEPENDENCIES

This program requires a working installation of Primer3 (tested with libprimer3 release
2.3.7).

This program was written for Pyhton v3.5+

This program was written in MacOS, and should work in any Unix/ Linux distribution.

——————————————————————————————————————————————————————————————————————————————————————————

2. CITE

Machado,D.J. (2018) Comparative Genomics and the Evolution of Amphibian Chemical Defense.
Ph.D. dissertation. Universidade de São Paulo, Programa de Pós-Graduação em Bioinformática.
